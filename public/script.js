document.addEventListener('DOMContentLoaded', function () {
    const countElement = document.getElementById('count');
    const incrementButton = document.getElementById('increment');
    const resetButton = document.getElementById('reset');

    let count = 0;

    function updateCount() {
        countElement.textContent = count;
    }

    incrementButton.addEventListener('click', function () {
        count++;
        updateCount();
    });

    resetButton.addEventListener('click', function () {
        count = 0;
        updateCount();
    });
});
